#!/bin/sh

URLS=${1}
MAX_TRIES=${2:-100}
TRY_DELAY=${3:-5}

export IFS=","
for URL in $URLS; do
  
  HOST=${URL%:*}
  PORT=${URL#*:}

  echo "Checking host ${HOST} port ${PORT}"

  counter=0

  while [ $counter -lt  ${MAX_TRIES} ]; do 
    echo ruok | nc ${HOST} ${PORT} | grep imok && break
    echo "Try ${counter}/${MAX_TRIES} failed, retrying after ${TRY_DELAY} seconds..."
    sleep ${TRY_DELAY}
    counter=$(( $counter + 1 ))
  done
done
