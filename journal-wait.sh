#!/bin/bash

URLS=${1}
MAX_TRIES=${2:-100}
TRY_DELAY=${3:-5}

export IFS=";"
for URL in $URLS; do
  echo "Checking url ${URL}"
  counter=0
  while [[ $counter -lt  ${MAX_TRIES} ]]; do
  	if [[ `(curl ${URL} || true) | grep "IPC"` ]]; then echo "${URL} is ready"; break; fi
    echo "Try ${counter}/${MAX_TRIES} failed, retrying after ${TRY_DELAY} seconds..."
    sleep ${TRY_DELAY}
    counter=$(( $counter + 1 ))
  done
done